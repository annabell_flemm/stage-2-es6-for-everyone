import View from './view';

class FighterView extends View {
  constructor(fighter, handleClick) {
    super();

    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter, handleClick) {
    const {
      name,
      source
    } = fighter;
    const nameElement = this.createName(fighter._id, name);
    const imageElement = this.createImage(source);

    this.element = this.createElement({
      tagName: 'div',
      className: 'fighter'
    });
    this.element.append(imageElement, nameElement);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
  }
  
  createName(id, name) {
    //add nameElement for saving   
    const nameElement = this.createElement({
      tagName: 'div',
      className: 'name'
    });
    nameElement.innerHTML = `
      <input id="checkbox-${id}" class="checkbox-custom" name="checkbox-${id}" type="checkbox" data-close="true" data-fighterid=${id}>
      <label for="checkbox-${id}" class="checkbox-custom-label" data-close="true">${name}</label>
      
      <div class="progress-wrapper hidden">
        <div class="progress-bar">
          <span class="progress-bar-fill"></span>
        </div>
      </div>
      `;

    nameElement.getElementsByTagName('input')[0].addEventListener('change', () => {
      const fightBtn = document.getElementsByClassName('fightBtn')[0];
      const fightBtnState = ['enabled', 'disabled'];
      const isEnabled = document.querySelectorAll('.checkbox-custom:checked').length === 2;

      fightBtn.classList.remove(fightBtnState[+isEnabled]);
      fightBtn.classList.add(fightBtnState[+!isEnabled]);
    });
    return nameElement;
  }

  createImage(source) {
    const attributes = {
      src: source
    };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }
}

export default FighterView;