import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';

class FightersView extends View {
  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  handleFighterClick(event, fighter) {
    const fightersDetailsMap = this.fightersDetailsMap;

    const createFighterModal = fighterDetail => {
      //create main element which will store all modal content
      const modal = this.createElement({
        tagName: 'div',
        className: 'modal'
      });
      //add modal-content to the modal
      modal.innerHTML =
        `<div class="modal-content">
        <span class="close" data-close="true">&times;</span>
        <h3>Fighter info</h3>
        <label for="name">Name</label>
        <input type="text" id="name${fighterDetail._id}" name="name" value="${fighterDetail.name}">
   
        <label for="health">Health</label>
        <input type="text" id="health${fighterDetail._id}" name="health" value="${fighterDetail.health}">
   
        <label for="attack">Attack</label>
        <input type="text" id="attack${fighterDetail._id}" name="attack" value="${fighterDetail.attack}">
   
        <label for="defense">Defense</label>
        <input type="text" id="defense${fighterDetail._id}" name="defense" value="${fighterDetail.defense}"> 
     
        <input type="submit" value="Submit" data-close="true">
      </div>`;

      //remove div with class 'modal'
      modal.getElementsByTagName('span')[0]
        .addEventListener('click', event => {
          const parent = event.target.parentNode.parentNode;
          parent.parentNode.removeChild(parent);
        });

      //add new data from modals inputs to fightersDetailsMap on 'submit'
      modal.querySelectorAll('input[type=submit]')[0]
        .addEventListener('click', () => {
          const id = fighterDetail._id;

          fighterDetail.name = document.getElementById(`name${id}`).value;
          fighterDetail.health = document.getElementById(`health${id}`).value;
          fighterDetail.attack = document.getElementById(`attack${id}`).value;
          fighterDetail.defense = document.getElementById(`defense${id}`).value;
          fightersDetailsMap.set(id, fighterDetail);
          modal.getElementsByTagName('span')[0].click();
        });

      return modal;
    };

    //showing modal method
    const showModal = (event, fighterDetail) => {
      const parent = document.querySelectorAll(`.fighter:nth-child(${fighterDetail._id})`)[0];
      if (event.target.dataset.close !== 'true' && parent.getElementsByClassName('modal').length === 0) {
        parent.append(createFighterModal(fighterDetail));
      }
    };

    /**
     * get data from server if it doesen`t exist and add to the fightersDetailsMap
     *  or get from the fightersDetailsMap */
    if (!fightersDetailsMap.has(fighter._id)) {
      fighterService.getFighterDetails(fighter._id)
        .then(data => {
          fightersDetailsMap.set(fighter._id, data);
          showModal(event, data);
        });
    } else {
      const fighterDetail = fightersDetailsMap.get(fighter._id);
      showModal(event, fighterDetail);
    }
  }
}

export default FightersView;