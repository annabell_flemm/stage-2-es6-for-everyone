import FightersView from './fightersView';
import { fighterService } from './services/fightersService';
import FightersBattle from './fightersBattle';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;
      const fightersBattle = new FightersBattle(App.rootElement, fightersView.fightersDetailsMap);

      App.rootElement.insertBefore(fightersElement, App.rootElement.childNodes[0]);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;