import View from './view';
import Fighter from './models/Fighter';

class FightersBattle extends View {
    checkeds = [];

    constructor(rootElement, fightersDetailsMap) {
        super();

        this.rootElement = rootElement;
        this.fightersDetailsMap = fightersDetailsMap;
        this.createFightButton();
    }

    //add fight button 
    createFightButton() {
        const fightBtn = this.createElement({
            tagName: 'img',
            className: 'fightBtn',
            attributes: {
                src: './resources/fight.png'
            }
        });
        //set button to disabled when selected less oe more than 2 fighters
        fightBtn.classList.add('disabled');
        fightBtn.addEventListener('click', () => {
            this.checkeds = document.querySelectorAll('.checkbox-custom:checked');
            this.checkeds.forEach(e => {
                let progress = e.parentElement.parentElement
                    .getElementsByClassName('progress-wrapper')[0];
                progress.classList.remove('hidden');
            });

            //create to instanses of class Fighter and start fight
            const fighter1 = new Fighter(this.fightersDetailsMap.get(this.checkeds[0].dataset.fighterid));
            const fighter2 = new Fighter(this.fightersDetailsMap.get(this.checkeds[1].dataset.fighterid));
            this.fight(fighter1, fighter2);
        });
        this.rootElement.append(fightBtn);
    }

    //function for fighting between two selected fighters
    fight(fighter1, fighter2) {
        //hide all fighers except selected
        const toggleFighters = () => {
            const fighters = document.getElementsByClassName('fighter');

            for (let j = fighters.length; j > 0; j--) {
                if (j != fighter1._id && j != fighter2._id) {
                    fighters[j - 1].classList.toggle('hidden');
                }
            }
        };
        toggleFighters();
        //add to second fighter turn to the opponent
        this.checkeds[1].parentElement.parentElement
            .getElementsByTagName('img')[0]
            .classList
            .add('flipped');

        //main function for one round of fight            
        let i = 0;
        const fighters = [fighter1, fighter2];
        const fightersHealth = [fighter1.health, fighter2.health];
        const round = roundNo => {
            fighter1.health -= fighter2.getHitPower() - fighter1.getBlockPower();
            fighter2.health -= fighter1.getHitPower() - fighter2.getBlockPower();
            this.checkeds.forEach((e, index) =>
                e.parentElement.parentElement
                .getElementsByClassName('progress-bar-fill')[0]
                .style
                .width = `${100 * fighters[index].health / fightersHealth[index]}%`);
            console.log(`Round #${roundNo} => ${fighter1.name} health: ${fighter1.health} vs ${fighter2.name} health: ${fighter2.health}`);
        };
        //set interval function to automate the fight
        const intervalId = setInterval(() => {
            if (fighter1.health > 0 && fighter2.health > 0) {
                round(++i);
            } else {
                /** clear interval to stop fight, 
                 * turn second fighter to normal state
                 * refresh health bar and hidden it */
                clearInterval(intervalId);
                this.checkeds[1].parentElement.parentElement
                    .getElementsByTagName('img')[0]
                    .classList
                    .remove('flipped');
                this.checkeds.forEach(e => {
                    let progress = e.parentElement.parentElement
                        .getElementsByClassName('progress-wrapper')[0];
                    progress.style.width = "100%";
                    progress.classList.add('hidden');
                });
                //show all fighters
                toggleFighters();
            
                alert(`${fighter1.health <= 0 ? fighter2.name : fighter1.name} wins!`);
            }
        }, 1000);
    }
}

export default FightersBattle;