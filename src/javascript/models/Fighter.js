class Fighter {
    constructor(fighter) {
        this._id = fighter._id;
        this.name = fighter.name;
        this.health = fighter.health;
        this.attack = fighter.attack;
        this.defense = fighter.defense;
    }

    _getRandom = () => Math.floor(Math.random() * 2) + 1;
    
    getHitPower() {
        const criticalHitChance = this._getRandom();
        return this.attack * criticalHitChance;
    }

    getBlockPower() {
        const dodgeChance = this._getRandom();
        return this.defense * dodgeChance;
    }
}

export default Fighter;